QUnit.test('only 2 or 4 players are accepted', function(assert) {
	var game = new Game(3);
	var undef = typeof game.pawns === 'undefined';
	assert.ok(undef, '3 players are not alloweed');
	game = new Game(2);
	assert.equal(game.pawns.length, 2, '2 players are alloweed');
	game = new Game(4);
	assert.equal(game.pawns.length, 4, '4 players are alloweed');
});

QUnit.test('put wall', function(assert) {
	var game = new Game(2);
	game.pawns = ['e1', 'e9'];
	game.putWall('k4v');
	assert.deepEqual(game.walls, [], 'k in unacceptable symbol');
	game.putWall('44v');
	assert.deepEqual(game.walls, [], 'the first symbol must be a letter');
	game.putWall('aav');
	assert.deepEqual(game.walls, [], 'the second symbol must be a digit');
	game.putWall('a3a');
	assert.deepEqual(game.walls, [], 'the third symbol must be "v" or "h"');
	game.putWall('a2v');
	assert.deepEqual(game.walls, ['a2v'], 'vertical wall was put on the board');
	game.putWall('a2v');
	assert.deepEqual(game.walls, ['a2v'], 'try to put wall again');
	game.putWall('a1v');
	assert.deepEqual(game.walls, ['a2v'], 'vertical and vertical intersection');
	game.putWall('a3v');
	assert.deepEqual(game.walls, ['a2v'], 'vertical and vertical intersection 2');
	game.putWall('a2h');
	assert.deepEqual(game.walls, ['a2v'], 'horizontal and vertical intersection');
	game.putWall('b4hh');
	assert.deepEqual(game.walls, ['a2v'], 'check long string');

	//Pawn is blocked, so wall disallowed
	game.pawns = ['a1'];
	game.walls = ['a1h'];
	game.putWall('b1v');
	assert.deepEqual(game.walls, ['a1h'], 'the wall blocks the pawn');
	//Try put walls. Must be 10.
	for (var i = game.chars.fv; i < game.chars.lv; i++)
		for (var j = game.chars.fh; j < game.chars.lh; j++) {
			game.whichTurn = 0;
			game.putWall(String.fromCharCode(i, j, 'h'.charCodeAt(0)));
		}
	assert.equal(game.walls.length, 10, '(first) number of walls is limited');
	for (var i = game.chars.fv; i < game.chars.lv; i++)
		for (var j = game.chars.fh; j < game.chars.lh; j++) {
			game.whichTurn = 1;
			game.putWall(String.fromCharCode(i, j, 'h'.charCodeAt(0)));
		}
	assert.equal(game.walls.length, 20, '(second) number of walls is limited');
});

QUnit.test('move pawn', function(assert) {
	var game = new Game(2);
	game.pawns[0] = 'e1';
	game.pawns[1] = 'e9';
	game.movePawn('e23');
	assert.equal(game.pawns[0], 'e1', 'check long string');
	game.movePawn('j2');
	assert.equal(game.pawns[0], 'e1', 'check disallowed chars');
	game.movePawn('ee');
	assert.equal(game.pawns[0], 'e1', 'check unacceptable char sequence');
	game.movePawn('e2');
	assert.equal(game.pawns[0], 'e2', 'move up');
	game.whichTurn = 0;
	game.movePawn('e1');
	assert.equal(game.pawns[0], 'e1', 'move down');
	game.whichTurn = 0;
	game.movePawn('d1');
	assert.equal(game.pawns[0], 'd1', 'move left');
	game.whichTurn = 0;
	game.movePawn('e1');
	assert.equal(game.pawns[0], 'e1', 'move right');
	game.whichTurn = 0;
	game.movePawn('e4');
	assert.equal(game.pawns[0], 'e1', 'not neightbor cell');
	game.movePawn('e0');
	assert.equal(game.pawns[0], 'e1', 'non-existing coordinate');

	game.putWall('e1h');
	game.movePawn('e2');
	assert.equal(game.pawns[0], 'e1', 'horizontal wall in front of pawn 1');

	game.walls = [];
	game.putWall('d1h');
	game.movePawn('e2');
	assert.equal(game.pawns[0], 'e1', 'horizontal wall in front of pawn 2');

	game.pawns[0] = 'e2';
	game.walls = [];
	game.putWall('e1v');
	game.movePawn('f2');
	assert.equal(game.pawns[0], 'e2', 'vertical wall in front of pawn 1');

	game.walls = [];
	game.putWall('e2v');
	game.movePawn('f2');
	assert.equal(game.pawns[0], 'e2', 'vertical wall in front of pawn 2');

	game.walls = [];
	game.pawns[0] = 'e1';
	game.pawns[1] = 'e2';
	game.movePawn('e2');
	assert.equal(game.pawns[0], 'e1', 'cell is occupied');
	game.movePawn('e3');
	assert.equal(game.pawns[0], 'e3', 'jump over enemy');
	game.whichTurn = 0;
	game.movePawn('e1');
	assert.equal(game.pawns[0], 'e1', 'jump back over enemy');
	game.putWall('d2h');
	game.whichTurn = 0;
	game.movePawn('e3');
	assert.equal(game.pawns[0], 'e1', 'wall prevents from jumping');
	game.movePawn('d2');
	assert.equal(game.pawns[0], 'd2', 'diagonal jumping');

	game.whichTurn = 0;
	game.pawns = ['e2', 'f2'];
	game.walls = ['f1v'];
	game.movePawn('f3');
	assert.equal(game.pawns[0], 'f3', 'diagonal jumping 2');

	game.whichTurn = 0;
	game.pawns = ['e1', 'e2'];
	game.walls = ['e2v', 'd2v'];
	game.movePawn('d2');
	assert.equal(game.pawns[0], 'e1', 'left diagonal is obstructed');
	game.movePawn('f2');
	assert.equal(game.pawns[0], 'e1', 'right diagonal is obstructed');
});

QUnit.test('AI tests', function(assert) {
	var game = new Game(2);
	var ai = new AIProxy(game, 0);

	game.pawns = ['e8', 'e3'];
	game.wallLimits = [0, 0];
	ai.searchMove();
	assert.equal(game.pawns[0], 'e9', 'Victory is achieved by the next move');

	game.pawns = ['e6', 'a8'];
	game.wallLimits = [0, 0];
	game.whichTurn = 0;
	ai.searchMove();
	assert.equal(game.pawns[0], 'e7', '(three steps) e7 leads to victory');
	game.movePawn('a7');
	ai.searchMove();
	assert.equal(game.pawns[0], 'e8', '(three steps) e8 leads to victory');
	game.movePawn('a6');
	ai.searchMove();
	assert.equal(game.pawns[0], 'e9', '(three steps) e9 is win!');

	game.pawns = ['b8', 'i9'];
	game.walls = ['b8h'];
	game.wallLimits = [0, 0];
	game.whichTurn = 0;
	ai.searchMove();
	assert.equal(game.pawns[0], 'a8', '(with wall) a8 leads to victory');
	game.movePawn('i8');
	ai.searchMove();
	assert.equal(game.pawns[0], 'a9', '(with wall) a9 is win!');

	game.pawns = ['e7', 'a2'];
	game.walls = ['c1h', 'e1h', 'g1h'];
	game.wallLimits = [1, 0];
	game.whichTurn = 0;
	ai.searchMove();
	assert.equal(game.walls[3],  'a1h', 'AI must put wall to block opponent')
});
